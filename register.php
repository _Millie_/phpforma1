

<?php


$imeErr = $prezimeErr = $datumRodjenjaErr = $adresaErr = $jmbgErr = $emailErr = $korisnickoImeErr = $lozinkaErr = $polErr ="";
$ime = $prezime = $datumRodjenja = $adresa = $jmbg = $email = $korisnickoIme = $lozinka = $pol = "";


$errors = false;
$fields = [];


//uslov- ime i prezime sadrze samo slova
function filterString($field){

    $field = filter_var(trim($field), FILTER_SANITIZE_STRING);


    if(filter_var($field, FILTER_VALIDATE_REGEXP, array("options"=>array("regexp"=>"/^[a-zA-Z\s]+/")))){
        return $field;
    }else{
        return FALSE;
    }
}

//validacija email adrese
function filterEmail($field){

    $email = filter_var(trim($field), FILTER_SANITIZE_EMAIL);

    return $email ? $email : false;
}

function checkEmailAddress ($email) {
    if (stristr($email,"@gmail.com",true)) {
        return true;
    }
    else {
        return false;
    }
}


//uslov- jmbg sadrzi samo brojeve
function filterNumber($field){

   // $is_valid = filter_var(trim($field), FILTER_SANITIZE_NUMBER_INT);
   $value = trim($field);
   is_numeric($value);

    return is_numeric($value) ? $value : false;
}


function filterDate($field){
    $date = filter_var(trim($field), FILTER_SANITIZE_STRING);

    return $date ? $date : false;
}



if($_SERVER["REQUEST_METHOD"] == "POST") {
    if (empty($_POST["ime"])) {
        $imeErr = 'Unesi ime.';
    } else {
        $ime = filterString($_POST["ime"]);
        $fields['Ime'] = $ime;
        if ($ime == false) {
            $imeErr = 'Ime ne sme da sadrzi brojeve.';
        }
        if (strlen($ime)<3){
            $imeErr = 'Unesite Vase puno ime!';
        }
    }

    
    if (empty($_POST["prezime"])) {
        $prezimeErr = 'Unesi prezime.';
    } else {
        $prezime = filterString($_POST["prezime"]);
        $fields['Prezime'] = $prezime;
        if ($prezime == false) {
            $prezimeErr = '<br>Prezime ne sme da sadrzi brojeve.';
        }
        elseif (strlen($prezime)<3){
            $prezimeErr = 'Unesite Vase puno prezime!';
        }
    }


    if (empty($_POST["jmbg"])) {
        $jmbgErr = 'Unesi jmbg.';
    } else {
        $jmbg = filterNumber($_POST["jmbg"]);
        //$fields['Jmbg'] = $jmbg;
        if ($jmbg == false) {
            $jmbgErr = '<br>Jmbg ne sme da sadrzi slova.';
        }
        else {
            if(strlen($jmbg)!=13){
                $jmbgErr = '<br>Jmbg mora da sadrzi tacno 13 cifara';
            }
    }

}

    if (empty($_POST["email"])) {
        $emailErr = 'Unesi email.';
    } else {
        $email = filterEmail($_POST["email"]);
        $fields['Email'] = $email;
        if ($email == false) {
            $emailErr = 'Unesi ispravnu email adresu.';
        }
        else {
            $email = checkEmailAddress ($_POST["email"]);
            $field['Email'] = $email;
        if ($email !== TRUE) {
            $emailErr = 'Unesi gmail adresu!';
        }
    
    }
}

    if (empty($_POST["datumRodjenja"])) {
        $datumRodjenjaErr = 'Unesi datum rodjenja.';
    } else {
        $datumRodjenja = date('Y-m-D', strtotime(filterDate($_POST["datumRodjenja"])));
        $fields['Datum Rodjenja'] = $datumRodjenja;
        if ($datumRodjenja == false) {
            $datumRodjenjaErr = 'Niste uneli datum.';
        }
    }


    if (empty($_POST["adresa"])) {
        $adresaErr = 'Unesi adresu.';
    } else {
        $adresa = ($_POST["adresa"]);
        $fields['Adresa'] = $adresa;
        if ($adresa == false) {
            $adresaErr = 'Niste uneli adresu.';
        }
    }


    if (empty($_POST["lozinka"])) {
        $lozinkaErr = 'Unesi lozinku.';
    } else {
        $lozinka = ($_POST["lozinka"]);
        $fields['Lozinka'] = $lozinka;
        if ($lozinka == false) {
            $lozinkaErr = 'Niste popunili ovo polje.';
        }
    }

    if (empty($_POST["korisnickoIme"])) {
        $korisnickoImeErr = 'Unesi korisnicko ime.';
    } else {
        $korisnickoIme = ($_POST["korisnickoIme"]);
        $fields['Korisnicko Ime'] = $korisnickoIme;
        if ($korisnickoIme == false) {
            $korisnickoImeErr = 'Unesi korisnicko ime.';
        }
    }


    if (empty($_POST["pol"])) {
        $polErr = 'Unesi pol.';
    } else {
        $pol = ($_POST["pol"]);
        $fields['Pol'] = $pol;
        if ($pol == false) {
            $polErr = 'Polje pol nije popunjeno';
        }
    }
}

if (!empty($imeErr) or !empty($prezimeErr) or !empty($datumRodjenjaErr) or !empty($adresaErr) or !empty($jmbgErr) or !empty($korisnickoImeErr)or !empty($lozinkaErr)or !empty($polErr)or !empty($emailErr)) {
    $params = "ime=" . urlencode($_POST["ime"]);
    $params .= "&prezime=" . urlencode($_POST["prezime"]);
    $params .= "&datumRodjenja=" . urlencode($_POST["datumRodjenja"]);
    $params .= "&adresa=" . urlencode($_POST["adresa"]);
    $params .= "&jmbg=" . urlencode($_POST["jmbg"]);
    $params .= "&email=" . urlencode($_POST["email"]);
    $params .= "&korisnickoIme=" . urlencode($_POST["korisnickoIme"]);
    $params .= "&lozinka=" . urlencode($_POST["lozinka"]);
    $params .= "&pol=" . urlencode($_POST["pol"]);


    $params .= "&ime=" . urlencode($imeErr);
    $params .= "&prezimeErr=" . urlencode($prezimeErr);
    $params .= "&datumRodjenjaErr=" . urlencode($datumRodjenjaErr);
    $params .= "&adresaErr=" . urlencode($adresaErr);
    $params .= "&jmbgErr=" . urlencode($jmbgErr);
    $params .= "&emailErr=" . urlencode($emailErr);
    $params .= "&korisnickoImeErr=" . urlencode($korisnickoImeErr);
    $params .= "&lozinkaErr=" . urlencode($lozinkaErr);
    $params .= "&polErr=" . urlencode($polErr);

    
    header("Location: index.php?" . $params);

  }  else {

    
    echo "<a href=\"index.php\">Nazad na pocetak</a>";
  }



  if (empty($error)) {
    foreach ($fields as $field_name => $field_value) {
        echo $field_name . ': ' . $field_value . '<br>';
    }
} else {
    
    header("Location: http://localhost/phpdomaciforme/index.php?error=invalid_parameters");
}
?>