<?php
$imeErr = $prezimeErr = $datumRodjenjaErr = $adresaErr = $jmbgErr = $emailErr = $korisnickoImeErr = $lozinkaErr = $polErr ="";
$ime = $prezime = $datumRodjenja = $adresa = $jmbg = $email = $korisnickoIme = $lozinka = $pol = "";



    if (isset($_GET['ime'])) { $ime = $_GET['ime']; }
    if (isset($_GET['prezime'])) { $prezime = $_GET['prezime']; }
    if (isset($_GET['datumRodjenja'])) { $datumRodjenja = $_GET['datumRodjenja']; }
    if (isset($_GET['adresa'])) { $adresa = $_GET['adresa']; }
    if (isset($_GET['jmbg'])) { $jmbg = $_GET['jmbg']; }
    if (isset($_GET['email'])) { $email = $_GET['email']; }
    if (isset($_GET['korisnickoIme'])) { $korisnickoIme = $_GET['korisnickoIme']; }
    if (isset($_GET['lozinka'])) { $lozinka = $_GET['lozinka']; }
    if (isset($_GET['pol'])) { $pol = $_GET['pol']; }


    if (isset($_GET['imeErr'])) { $imeErr = $_GET['imeErr']; }
    if (isset($_GET['prezimeErr'])) { $prezimeErr = $_GET['prezimeErr']; }
    if (isset($_GET['datumRodjenjaErr'])) { $datumRodjenjaErr = $_GET['datumRodjenjaErr']; }
    if (isset($_GET['adresaErr'])) { $adresaErr = $_GET['adresaErr']; }
    if (isset($_GET['jmbgErr'])) { $jmbgErr = $_GET['jmbgErr']; }
    if (isset($_GET['emailErr'])) { $emailErr = $_GET['emailErr']; }
    if (isset($_GET['korisnickoImeErr'])) { $korisnickoImeErr = $_GET['korisnickoImeErr']; }
    if (isset($_GET['lozinkaErr'])) { $lozinkaErr = $_GET['lozinkaErr']; }
    if (isset($_GET['polErr'])) { $polErr = $_GET['polErr']; }

?>






<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Milena Miletic - <?php echo Date("d.m.Y.") ?></title>
    <link rel="stylesheet" type="text/css" media="screen" href="css/styless.css" />
    <style type="text/css">
        .error{ color: pink; }
        </style>
</head>


<body>

<form class="form" action="register.php" method="post">
<p><span class="error">* Obavezna Polja.</span></p>




<label for="inputime">Ime:<sup>*</sup></label>
<input type="text" name="ime" id="inputime" value="<?php echo $ime; ?>">
<span class="error"><?php echo $imeErr;?></span></br></br>



<label for="inputprezime">Prezime:<sup>*</sup></label>
<input type="text" name="prezime" id="inputprezime" value="<?php echo $prezime; ?>" >
<span class="error"><?php echo $prezimeErr;?></span></br></br>


<label for="inputdatumRodjenja">Datum Rodjenja:<sup>*</sup></label>
<input type="date" name="datumRodjenja" id="inputdatumRodjenja" value="<?php echo $datumRodjenja; ?>">
<span class="error"><?php echo $datumRodjenjaErr;?></span></br></br>

<label for="inputadresa">Adresa:<sup>*</sup></label>
<input type="text" name="adresa" id="inputadresa" value="<?php echo $adresa; ?>">
<span class="error"><?php echo $adresaErr;?></span></br></br>

<label for="inputjmbg">Jmbg:<sup>*</sup></label>
<input type="text" name="jmbg" id="inputjmbg" value="<?php echo $jmbg; ?>">
<span class="error"><?php echo $jmbgErr;?></span></br></br>

<label for="inputemail">Email:<sup>*</sup></label>
<input type="text" name="email" id="inputemail" value="<?php echo $email; ?>">
<span class="error"><?php echo $emailErr;?></span></br></br>

<label for="inputkorisnickoIme">Korisnicko Ime:<sup>*</sup></label>
<input type="text" name="korisnickoIme" id="inputkorisnickoIme" value="<?php echo $korisnickoIme; ?>">
<span class="error"><?php echo $korisnickoImeErr;?></span></br></br>

<label for="inputlozinka">Lozinka:<sup>*</sup></label>
<input type="text" name="lozinka" id="inputlozinka" value="<?php echo $lozinka; ?>">
<span class="error"><?php echo $lozinkaErr;?></span></br></br>

<label for="inputpol">Pol:<sup>*</sup></label>
<input type="radio" name="pol" <?php if(isset($pol) && $pol=="zenski") echo "checked";?> value="zenski">Zenski
<input type="radio" name="pol" <?php if(isset($pol) && $pol=="muski") echo "checked";?> value="zenski">Muski
<input type="radio" name="pol" <?php if(isset($pol) && $pol=="drugo") echo "checked";?> value="zenski">Drugo
<span class="error"><?php echo $polErr;?></span></br></br>

<input type="submit" value="Posalji">
    </form>
</body>

</html>
